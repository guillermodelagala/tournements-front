import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { HomeComponent } from './components/home.component';
import { ResultComponent } from './components/result.component';
import { TournementComponent } from './components/tournement.component';
import { UserComponent } from './components/user.component';
import { PointComponent } from './components/point.component';
import { ErrorComponent } from './components/error.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'home', component: HomeComponent},
    {path: 'results', component: ResultComponent},
    {path: 'tournement', component: TournementComponent},
    {path: 'user', component: UserComponent},
    {path: 'point', component: PointComponent},
    {path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);