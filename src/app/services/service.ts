import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { User } from '../models/user';
import { Tournement } from '../models/tournement';
import { Point } from '../models/point';
import { Result } from '../models/result';
import { GLOBAL} from './global';

@Injectable()
export class Service{
    public url: string;

    constructor(
        public _http: Http
    ){
        this.url = GLOBAL.url;
    }

    getUsers(){
        return this._http.get(this.url+'users').map(res => res.json());
    }

    getTournements(){
        return this._http.get(this.url+'tournements').map(res => res.json());
    }

    getPoints(){
        return this._http.get(this.url+'points').map(res => res.json());
    }

    getResults(){
        return this._http.get(this.url+'results').map(res => res.json());
    }

    saveResults(results: Result[]){
        let headers = new Headers({'Content-Type':'application/json;charset=UTF-8', 'Accept' : 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this._http.post(this.url+'save-results', results, options).subscribe(
            data  => {                
                console.log("POST Request is successful ", data);                
                if (data.status === 200) {
                    alert('SUCCESS!! :-)\n\n');
                }
            },
            error  => {            
                console.log("Error", error);
                alert('ERROR!! :-()\n\n');            
            }            
        );
    }

    saveTournement(tournement: Tournement){
        let headers = new Headers({'Content-Type':'application/json;charset=UTF-8', 'Accept' : 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this._http.post(this.url+'save-tournement', tournement, options).subscribe(
            data  => {                
                console.log("POST Request is successful ", data);
                if (data.status !== 200) {
                    console.log("Error", data);
                    alert('ERROR!! :-()\n\n'); 
                }
            },
            error  => {            
                console.log("Error", error);
                alert('ERROR!! :-()\n\n');            
            }            
        );
    }

    deleteTournement(tournement: Tournement){
        let headers = new Headers({'Content-Type':'application/json;charset=UTF-8', 'Accept' : 'application/json'});
        return this._http.delete(this.url+'delete-tournement', new RequestOptions({
            headers: headers,
            body: tournement})).subscribe(
            data  => {                
                console.log("Delete Request is successful ", data);
                if (data.status !== 200) {
                    console.log("Error", data);
                    alert('ERROR!! :-()\n\n'); 
                }
            },
            error  => {            
                console.log("Error", error);
                alert('ERROR!! :-()\n\n');            
            }            
        );
    }

    saveUser(user: User){
        let headers = new Headers({'Content-Type':'application/json;charset=UTF-8', 'Accept' : 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this._http.post(this.url+'save-user', user, options).subscribe(
            data  => {                
                console.log("POST Request is successful ", data);
                if (data.status !== 200) {
                    console.log("Error", data);
                    alert('ERROR!! :-()\n\n'); 
                }
            },
            error  => {            
                console.log("Error", error);
                alert('ERROR!! :-()\n\n');            
            }            
        );
    }

    deleteUser(user: User){
        let headers = new Headers({'Content-Type':'application/json;charset=UTF-8', 'Accept' : 'application/json'});
        return this._http.delete(this.url+'delete-user', new RequestOptions({
            headers: headers,
            body: user})).subscribe(
            data  => {                
                console.log("Delete Request is successful ", data);
                if (data.status !== 200) {
                    console.log("Error", data);
                    alert('ERROR!! :-()\n\n'); 
                }
            },
            error  => {            
                console.log("Error", error);
                alert('ERROR!! :-()\n\n');            
            }            
        );
    }
}