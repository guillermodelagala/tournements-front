import { Component } from '@angular/core';
import { Service } from '../services/service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tournement } from '../models/tournement';

@Component({
    selector: 'tournement',
    templateUrl: '../views/tournement.html',
    providers: [Service],
    styleUrls: ['../styles/tournement.style.css'] 
})

export class TournementComponent{
    public titulo: string;
    public tournements: Tournement[];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: Service
    ){
        this.titulo = 'Torneos';
    }

    ngOnInit(){
        this._service.getTournements().subscribe(
            data => {
                this.tournements = data;
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    editRow(e, index) {
        this.tournements[index].isEditable = true;
    }

    deleteRow(e, index) {        
        this._service.deleteTournement(this.tournements[index]);
        this.tournements.splice(index, 1);
    }

    saveRow(e, index) {
        this.tournements[index].isEditable = false;
        if (this.tournements[index].name) {
            this._service.saveTournement(this.tournements[index]);
        }
    }

    newRow() {
        this.tournements.push(new Tournement(this.tournements.length+1,null,true));        
    }
}