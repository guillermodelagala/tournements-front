import { Component } from '@angular/core';
import { Service } from '../services/service';
import { ActivatedRoute, Router } from '@angular/router';
import { Point } from '../models/point';

@Component({
    selector: 'point',
    templateUrl: '../views/point.html',
    providers: [Service],
    styleUrls: ['../styles/point.style.css']
})

export class PointComponent{
    public titulo: string;
    public points: Point[];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: Service
    ){
        this.titulo = 'Puntos';
    }

    ngOnInit(){        
        this._service.getPoints().subscribe(
            data => {
                this.points = data;
            },
            error => {
                console.log(<any>error);
            }
        );

    }
}