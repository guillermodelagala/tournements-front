import { Component } from '@angular/core';
import { Service } from '../services/service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { Tournement } from '../models/tournement';
import { Point } from '../models/point';
import { Result } from '../models/result';
import { UserData } from '../models/user.data';

@Component({
    selector: 'home',
    templateUrl: '../views/home.html',
    providers: [Service]
})

export class HomeComponent{
    public titulo: string;
    public users: User[];
    public tournements: Tournement[];
    public points: Point[];
    public results: Result[];
    public userDataList: UserData[];
    public orderName: number;
    public orderTotal: number;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: Service
    ){
        this.titulo = 'Clasificación general';
    }

    ngOnInit(){
        this._service.getTournements().subscribe(
            data => {
                // Torneos
                this.tournements = data;
                
                this._service.getUsers().subscribe(
                    data => {
                        //Usuarios
                        this.users = data;
                        this.users.sort((a,b) => a.name.localeCompare(b.name));

                        this._service.getPoints().subscribe(
                            data => {
                                //Puntos
                                this.points = data;

                                this._service.getResults().subscribe(
                                    // Resultados
                                    data => {
                                        this.results = data;
                                        console.log(this.results);
                                        this.buildData();
                                    },
                                    error => {
                                        console.log(<any>error);
                                    }
                                );
                            },
                            error => {
                                console.log(<any>error);
                            }
                        );
                    },
                    error => {
                        console.log(<any>error);
                    }
                );
            },
            error => {
                console.log(<any>error);
            }
        );        
    }

    buildData(){
        //ORDENAR POR NOMBRE
        //this.tournements.sort((a,b) => a.name.localeCompare(b.name));
        this.userDataList = [];
        this.users.forEach(user => {
            let points = new Array();
            let total = 0;
            this.tournements.forEach(tournement => {
                let tournementPoint = 0;
                this.results.forEach(result => {
                    if (result.idUser === user.id && result.idTournement === tournement.id) {
                        this.points.forEach(points => {
                            if (points.id === result.idPoint){
                                tournementPoint = points.points;
                            }
                        });
                    }
                });
                total += tournementPoint;
                points.push(tournementPoint);
            });
            this.userDataList.push(new UserData(user.name, points, total));
        });
        this.sortByTotal();
        //this.userDataList.sort((a,b) => a.name.localeCompare(b.name));

        
        //this.userDataList.sort((a,b) => b.total-a.total);
        console.log(this.userDataList);
    }

    sortByName() {
        //Sort by name
        if (this.orderName === 0) {
            this.userDataList.sort((a,b) => b.name.localeCompare(a.name));
            this.orderName = 1;
        } else {
            this.userDataList.sort((a,b) => a.name.localeCompare(b.name));            
            this.orderName = 0;
        }
        this.orderTotal = -1;     
    }

    sortByTotal() {
        //Sort by total
        if (this.orderTotal === 0) {
            this.userDataList.sort((a,b) => a.total-b.total);
            this.orderTotal = 1;
        } else {
            this.userDataList.sort((a,b) => b.total-a.total);            
            this.orderTotal = 0;
        }
        this.orderName = -1;     
    }

}