import { Component } from '@angular/core';
import { Service } from '../services/service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';

@Component({
    selector: 'user',
    templateUrl: '../views/user.html',
    providers: [Service],
    styleUrls: ['../styles/user.style.css'] 
})

export class UserComponent{
    public titulo: string;
    public users: User[];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: Service
    ){
        this.titulo = 'Usuarios';
    }

    ngOnInit(){
        this._service.getUsers().subscribe(
            data => {
                this.users = data;
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    editRow(e, index) {
        this.users[index].isEditable = true;
    }

    deleteRow(e, index) {        
        this._service.deleteUser(this.users[index]);
        this.users.splice(index, 1);
    }

    saveRow(e, index) {
        this.users[index].isEditable = false;
        if (this.users[index].name) {
            this._service.saveUser(this.users[index]);
        }
    }

    newRow() {
        this.users.push(new User(this.users.length+1,null,true));        
    }
}