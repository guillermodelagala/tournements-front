import { Component } from '@angular/core';
import { Service } from '../services/service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { Tournement } from '../models/tournement';
import { Point } from '../models/point';
import { Result } from '../models/result';

@Component({
    selector: 'home',
    templateUrl: '../views/result.html',
    providers: [Service],
    styleUrls: ['../styles/result.style.css']
})

export class ResultComponent{
    public titulo: string;
    public users: User[];
    public tournements: Tournement[];
    public points: Point[];
    public results: Result[];
    private sendResults: Result[];
    public idTournement: number;
    public msgError: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: Service
    ){
        this.titulo = 'Resultados';
    }

    ngOnInit(){
        this.idTournement = 0;
        this.results = new Array(new Result(null,null,null,null));

        this._service.getTournements().subscribe(
            data => {
                // Torneos
                this.tournements = data;
                this.tournements.sort((a,b) => a.name.localeCompare(b.name));

                this._service.getUsers().subscribe(
                    data => {
                        //Usuarios
                        this.users = data;
                        this.users.sort((a,b) => a.name.localeCompare(b.name));

                        this._service.getPoints().subscribe(
                            data => {
                                //Puntos
                                this.points = data;

                                //Initial rows
                                for (let i=0; i<this.users.length; i++) {
                                    this.newRow();
                                }
                            },
                            error => {
                                console.log(<any>error);
                            }
                        );
                    },
                    error => {
                        console.log(<any>error);
                    }
                );
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    newRow() {
        this.results.push(new Result(null,null,null,null));        
    }

    deleteRow(e, index) {
        this.results.splice(index, 1);      
    }

    onSubmit() {
        if (this.idTournement == null || this.idTournement == 0){
            this.msgError = "Debes de seleccionar un torneo*";
            alert(this.msgError);
        } else {
            this.sendResults = []
            this.results.forEach((result, index) => {
                if (result.idUser &&  result.idPoint){
                    result.idTournement = this.idTournement;
                    this.sendResults.push(result);                    
                } else {
                    this.msgError = "No se enviaron todos los resultados. Alguna fila no se ha rellenado";
                    //alert(this.msgError);
                }
            });
            console.log(this.sendResults);
            this._service.saveResults(this.sendResults);
            // TODO empty if success
            //this.results = [];
        }        
    }
}