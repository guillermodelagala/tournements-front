export class UserData {
    constructor(
        public name: string,
        public points: number[],
        public total: number
    ){}
}