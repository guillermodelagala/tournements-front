export class Tournement {
    constructor(
        public id: number,
        public name: string,
        public isEditable: boolean
    ){}
}