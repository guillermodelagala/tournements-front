export class Result {
    constructor(
        public id: number,
        public idTournement: number,
        public idUser: number,
        public idPoint: number
    ){}
}